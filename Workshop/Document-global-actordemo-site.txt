# **ActorDemo site**
**ActorDemo** is a simple site where you can use an AI service to transform images.
The site can be configured to use authorization. If authorization is turned on, the user must login to be able to use the service.
