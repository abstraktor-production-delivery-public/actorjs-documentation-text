# **2) Mock with HTTP**

---
## ***Goto the Image AI page***

---
#### **Purpose**
Learn how to ***mock*** with the [[REF=, STACK_HTTP]] stack. ***Mock*** with a [[REF=, ABS_Terminating_Actor]].
Learn how to use ***[[REF=, ABS_Content]]*** with the [[REF=, API_Actor_Content]].

---
#### **Task**
1) Mock ***ImageService*** and reply with  ***Justin***.
2) Load the ***ActorDemo*** site and click the **"Goto:** Image AI**"** button.

---
#### **Analyze**
```node
Config(nodeWidth: 80, nodeHeight: 50, nodeWidthBetween: 75, nodeHeightBetween: 25, widthBias: 30, heightBias: 30, border: true, backgroundColor: default)

Nodes[Browser(; ; White), ActorDemo(; ; Honeydew), ImageService(; ; White)]


Browser => ActorDemo[http]: http
ActorDemo => ImageService[http]: http
```

The ***Image AI*** page uses the backend node ImageService. 
* [[REF=, STACK_HTTP]] is the protocol between the ***Browser*** and ***ActorDemo*** so we will use the [[REF=, STACK_PUPPETEER]] stack to test this page.
* [[REF=, STACK_HTTP]] is the protocol between the ***ActorDemo*** and ***ImageService*** so we will use the [[REF=, STACK_HTTP]] stack to ***mock*** the ***ImageService***.

  * The ***ImageService*** has two **REST API** methods.
  * 1) **AiGetImages**:
      1.1) **protocol**: HTTP
      1.2) **Method**: GET
      1.3) **url**: api/v1/image/get-images
      1.4) **returns**: [[REF=, MDN_JSON]] - **content-type**: application/json
        * [[REF=, MDN_objects]]: *Array of objects with image & AI data*
        * **name**:  [[REF=, MDN_string]]: *Name of the image*
        * **mime**:  [[REF=, MDN_string]]: *Image mime type*
        * **aix**: [[REF=, MDN_strings]]: *Array of AI transformations*
   * 2) **AiGetImage**
      2.1) **protocol**: HTTP
      2.2) **Method**: GET
      2.3) **url**: api/v1/image/get-image/[**name**]
        &nbsp; &nbsp; 2.3.1) **name**: *the name of the AI instance.*
      1.4) **returns**: Image - **content-type**: image/[mime]

```seq
Config(nodeWidth: 280, nodeMessageHeight: 17, nodeEventHeight: 10, nodeCommentHeight: 26, widthBias: 60, heightBias: 30, lineNumbers: false, border: true, backgroundColor: #EAFEEA)

Nodes[Browser, ActorDemo, ImageService]
ActorDemo -o ImageService[http]: 
Browser browser [puppeteer]: srcActorDemo
Browser page [puppeteer]: page1
Browser -o ActorDemo[http]: 
Browser => ActorDemo[http]: GET http://localhost:9045/start
ActorDemo => Browser[http]: 200 OK
Browser /**/ ActorDemo[http]: Loading site (js, css, images, REST API, ... .)
Browser click [puppeteer]: id = #from_start_to_image_ai
Browser => ActorDemo[http]: POST http://actordemo:9045/abs-data/AiGetImages
ActorDemo => ImageService[http]: GET api/v1/image/get-images
ImageService => ActorDemo[http]: 200 OK
ActorDemo => Browser[http]: 200 OK
Browser => ActorDemo[http]: POST http://actordemo:9045/abs-data/AiGetImage
ActorDemo => ImageService[http]: GET api/v1/image/get-image/Justin
ImageService => ActorDemo[http]: 200 OK
ActorDemo => Browser[http]: 200 OK
ImageService -x ActorDemo[http]: 
Browser -x ActorDemo[http]: 
```

---
#### **Conclusion**
1) Create an [[REF=, ABS_Originating_Actor]] with the [[REF=, STACK_PUPPETEER]] stack.
  1.1 **Click** the button with ***selector***: #from_start_to_image_ai
2) Create a [[REF=, ABS_Terminating_Actor]] with the [[REF=, STACK_HTTP]] stack.
  2.1 Answer the [[REF=, STACK_HTTP]] REST API ***request*** AiGetImages. Answer with one image: [{name:'Justin',mime:'images/jpeg',ais:['background', 'shirt', 'sunglasses']}]
  2.2 Answer the [[REF=, STACK_HTTP]] REST API ***request*** AiGetImage/Justin. Answer with the image requested.

---
#### **Solution**
+ Test Case: [GotoImageAI](/../test-cases/ActorDemo/ImageIA/GotoImageAI)
