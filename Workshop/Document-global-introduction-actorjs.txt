# **Introduction - ActorJs**

<div style="position:relative;display:flex;flex-direction:column;float:right">
  <h4 style="width: 233px; margin: 0px 16px 10px; font-weight:bold;">Core Tool - Repos</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/repos.png" style="width: 233px; margin: 0px 16px 10px;">
  <h4 style="width: 233px; margin: 2px 16px 10px; font-weight:bold;">Core Tool - SUTs</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/sut.png" style="width: 233px; margin: 0px 16px 10px;">
  <h4 style="width: 233px; margin: 0px 16px 10px; font-weight:bold;">Core Tool - Test Suits</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/test-suite-execution.png" style="width: 233px; margin: 0px 16px 10px;">
  <h4 style="width: 233px; margin: 0px 16px 10px; font-weight:bold;">Core Tool - Test Cases</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/test-case-definition.png" style="width: 233px; margin: 0px 16px 10px;">
  <h4 style="width: 233px; margin: 0px 16px 10px; font-weight:bold;">Core Tool - Actors</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/actors.png" style="width: 233px; margin: 0px 16px 10px;">
  <h4 style="width: 233px; margin: 0px 16px 10px; font-weight:bold;">Core Tool - Stacks</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/stacks.png" style="width: 233px; margin: 0px 16px;">
  <h4 style="width: 233px; margin: 0px 16px 10px; font-weight:bold;">Data Tool - Address</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/address.png" style="width: 233px; margin: 0px 16px;">
  <h4 style="width: 233px; margin: 0px 16px 10px; font-weight:bold;">Data Tool - Test Data</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/test-data.png" style="width: 233px; margin: 0px 16px;">
  <h4 style="width: 233px; margin: 0px 16px 10px; font-weight:bold;">Data Tool - Content</h3>
  <img class="markup_image" onclick="if('100%' === this.style.width) {this.style.width = '233px';} else {this.style.width = '100%';}" src="/abs-images/images/visualization/content.png" style="width: 233px; margin: 0px 16px;">
</div>

ActorJs is an integrated test - development environment. It contains tools, organizes data, and provides several views that visualize logs, sequence diagrams, and more in real time.

## **Core Tools**
* [[REF=, ABS_Repos]] - manages your [[REF=, ABS_Repos]].
* [[REF=, ABS_SUTs]] - manages your [[REF=, ABS_SUTs]].
* [[REF=, ABS_Test_Suites]] - manages and execute [[REF=, ABS_Test_Suites]].
* [[REF=, ABS_Test_Cases]] - manages and execute [[REF=, ABS_Test_Cases]].
* [[REF=, ABS_Stacks]] - editor to build [[REF=, ABS_Stacks]].
* [[REF=, ABS_Actors]] - editor to build [[REF=, ABS_Actors]].

## **Data Tools**
* [[REF=, ABS_Addressing_Address]] - manages your [[REF=, ABS_Addressing]].
* [[REF=, ABS_Test_Data]] - manages your [[REF=, ABS_Test_Data]].
* [[REF=, ABS_Content]] - manages your [[REF=, ABS_Content]].
* [[REF=, ABS_Load]] - manages your [[REF=, ABS_Load]]. `Coming soon`

## **Visualization Tools**
* Log
* Sequence Diagram
