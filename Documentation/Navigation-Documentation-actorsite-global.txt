[
  {
    "heading": "Introduction",
    "refs": [
      {
        "name": "Be 100% more efficient",
        "link": "Be-100-more-efficient"
      },
      {
        "name": "Trigger, Mock, and Intercept anything",
        "link": "actorjs-trigger-mock-and-intercept-anything"
      },
      {
        "name": "Advanced Visualization",
        "link": "advanced-visualization"
      },
      {
        "name": "Debugging",
        "link": "debugging"
      },
      {
        "name": "The Development Environment",
        "link": "the-development-environment"
      },
      {
        "name": "The Test Environment",
        "link": "the-test-environment"
      },
	  {
        "name": "Divide and Reuse",
        "link": "divide-and-reuse"
      },
      {
        "name": "The Architecture",
        "link": "the-architecture"
      },
      {
        "name": "Why ActorJs?",
        "link": "why-actorjs"
      },
      {
        "name": "Think Ahead",
        "link": "think-ahead"
      }
    ]
  },
  {
    "heading": "API:s",
    "refs": [
      {
        "name": "Actor API/Actor",
        "link": "actor-api/actor"
      },
      {
        "name": "Actor API/ActorPart",
        "link": "actor-api/actor-part"
      },
      {
        "name": "Actor API/Async",
        "link": "actor-api/async"
      },
      {
        "name": "Actor API/Client Stack",
        "link": "actor-api/client-stack"
      },
      {
        "name": "Actor API/Content",
        "link": "actor-api/content"
      },
      {
        "name": "Actor API/Logger",
        "link": "actor-api/logger"
      },
      {
        "name": "Actor API/Process",
        "link": "actor-api/process"
      },
      {
        "name": "Actor API/Server Stack",
        "link": "actor-api/server-stack"
      },
      {
        "name": "Actor API/Shared Data",
        "link": "actor-api/shared-data"
      },
      {
        "name": "Actor API/Test Data",
        "link": "actor-api/test-data"
      },
      {
        "name": "Actor API/Timer",
        "link": "actor-api/timer"
      },
      {
        "name": "Actor API/Verification Data",
        "link": "actor-api/verification-data"
      },
      {
        "name": "ConnectionWorker API/Logger",
        "link": "connection-worker-api/logger"
      },
      {
        "name": "Nodejs API/Fs",
        "link": "nodejs-api/fs"
      },
      {
        "name": "Stack API/AsciiDictionary",
        "link": "stack-api/ascii-dictionary"
      },
      {
        "name": "Stack API/BitByte",
        "link": "stack-api/bit-byte"
      },
      {
        "name": "Stack API/BinaryLog",
        "link": "stack-api/binary-log"
      },
      {
        "name": "Stack API/ContentBase",
        "link": "stack-api/content-base"
      },
      {
        "name": "Stack API/IpLog",
        "link": "stack-api/ip-log"
      },
      {
        "name": "Stack API/LogInner",
        "link": "stack-api/log-inner"
      },
      {
        "name": "Stack API/LogPartText",
        "link": "stack-api/log-part-text"
      },
      {
        "name": "Stack API/LogPartError",
        "link": "stack-api/log-part-error"
      },
      {
        "name": "Stack API/LogPartRef",
        "link": "stack-api/log-part-ref"
      }
    ]
  },
  {
    "heading": "Stacks",
    "refs": [
      {
        "name": "[[stack]]",
        "link": ""
      }
    ]
  },
  {
    "heading": "Abstractions",
    "refs": [
      {
        "name": "Addressing",
        "link": "abstractions/addressing"
      },
      {
        "name": "Actor",
        "link": "abstractions/actor/actor"
      },
      {
        "name": "- Actor Originating",
        "link": "abstractions/actor/actor-originating"
      },
      {
        "name": "- Actor Terminating",
        "link": "abstractions/actor/actor-terminating"
      },
      {
        "name": "- Actor Proxy",
        "link": "abstractions/actor/actor-proxy"
      },
      {
        "name": "- Actor Local",
        "link": "abstractions/actor/actor-local"
      },
      {
        "name": "- Actor Condition",
        "link": "abstractions/actor/actor-condition"
      },
      {
        "name": "ActorPart",
        "link": "abstractions/actorpart/actorpart"
      },
      {
        "name": "Content",
        "link": "abstractions/content"
      },
      {
        "name": "Function Test",
        "link": "abstractions/functions-test"
      },
      {
        "name": "Function Under Test",
        "link": "abstractions/functions-unde-test"
      },
      {
        "name": "Load",
        "link": "abstractions/load-test"
      },
      {
        "name": "Repo",
        "link": "abstractions/repo"
      },
      {
        "name": "Shared Data",
        "link": "abstractions/shared-data"
      },
      {
        "name": "Stack",
        "link": "abstractions/stack/stack"
      },
      {
        "name": "- Stack Connection",
        "link": "abstractions/stack/connection"
      },
	  {
        "name": "- Stack Server Connection",
        "link": "abstractions/stack/server-connection"
      },
      {
        "name": "- Stack Client Connection",
        "link": "abstractions/stack/client-connection"
      },
      {
        "name": "- Stack Encoder",
        "link": "abstractions/stack/encoder"
      },
      {
        "name": "- Stack Decoder",
        "link": "abstractions/stack/decoder"
      },
      {
        "name": "- Stack Message",
        "link": "abstractions/stack/message"
      },
      {
        "name": "- Stack Worker",
        "link": "abstractions/stack/worker"
      },
      {
        "name": "System Configuration Floor",
        "link": "abstractions/configuration-floor"
      },
      {
        "name": "System To Test",
        "link": "abstractions/system-to-test"
      },
      {
        "name": "System Under Test",
        "link": "abstractions/system-under-test"
      },
      {
        "name": "Test Case",
        "link": "abstractions/test-case/test-case"
      },
      {
        "name": "Test Data",
        "link": "abstractions/test-data"
      },
      {
        "name": "Test Suite",
        "link": "abstractions/test-suite"
      },
      {
        "name": "Verification Data",
        "link": "abstractions/verification-data"
      }
    ]
  },
  {
    "heading": "Markup",
    "refs": [
      {
        "name": "Introduction",
        "link": "markup-markup"
      },
      {
        "name": "Simple/Api-Status",
        "link": "markup-api-status"
      },
      {
        "name": "Simple/Anchor",
        "link": "markup-anchor"
      },
      {
        "name": "Simple/Documentation-Status",
        "link": "markup-documentation-status"
      },
      {
        "name": "Simple/Embed",
        "link": "markup-embed"
      },
      {
        "name": "Simple/I.e.",
        "link": "markup-ie"
      },
      {
        "name": "Simple/Note",
        "link": "markup-note"
      },
      {
        "name": "Simple/Reference",
        "link": "markup-ref"
      },
      {
        "name": "Complex/Chart/Line",
        "link": "markup-chart-line"
      },
      {
        "name": "Complex/Document",
        "link": "markup-documentation"
      },
      {
        "name": "Complex/Flowchart",
        "link": "markup-flowchart"
      },
      {
        "name": "Complex/Objects",
        "link": "markup-html"
      },
      {
        "name": "Complex/Image",
        "link": "markup-image"
      },
      {
        "name": "Complex/Lab",
        "link": "markup-lab"
      },
      {
        "name": "Complex/Navigation",
        "link": "markup-navigation"
      },
      {
        "name": "Complex/Node Diagram",
        "link": "markup-node-diagram"
      },
      {
        "name": "Complex/Sequence Diagram",
        "link": "markup-sequence-diagram"
      },
      {
        "name": "Complex/State Machine",
        "link": "markup-state-machine"
      },
      {
        "name": "Complex/Table",
        "link": "markup-table"
      },
      {
        "name": "Complex/Test Case",
        "link": "markup/markup-test-case"
      },
      {
        "name": "Complex/Test Suite",
        "link": "markup/markup-test-suite"
      }
    ]
  }
]