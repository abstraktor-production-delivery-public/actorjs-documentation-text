# **Test Suite Abstraction**

## **Description**


[[ANCHOR={"id":"7f833cd6-1770-45b8-96b9-32423f91fbda","visible":true}]]
[[NOTE={"guid":"4bcabc8f-99ca-487d-9c1c-a0d0d0a788e2"}]]
### **Creating a Test Suite**
[[ANCHOR={"id":"5c3f4148-fb0d-42fc-9984-0d86e3f51848","visible":true}]]
[[NOTE={"guid":"87a9842a-16a2-4eaa-99d4-29ea7a3c50f8"}]]
#### **Table**
To Create a [[REF=, ABS_Test_Suite]] it is just to create a table with the [[REF=, ABS_Test_Case]]s and [[REF=, ABS_Test_Suite]]s to be executed.
```ts
|TestSuiteAbstraction                                               |
|name                                          |execution|iterations|
|tc.Actor.Demo.DemoMobileBrowsingLogin         |         |          |
|tc.Actor.Demo.DemoMobileBrowsingGetImage      |         |          |
|tc.Actor.Demo.DemoMobileBrowsingRightsGetImage|         |          |
|tc.Actor.Demo.DemoMobileBrowsingImageBmp      |         |          |
|tc.Actor.Demo.DemoMobileBrowsingImageGif      |         |          |
|tc.Actor.Demo.DemoMobileBrowsingImageJpg      |         |          |
```
[[ANCHOR={"id":"322e8788-50aa-4d9b-889e-96a0ba2d70cf","visible":true}]]
[[NOTE={"guid":"d6466c03-7924-44b8-8500-9ddbb19ffc3c"}]]
#### **Markup**
Editing the tables will be done in a markup text format. This means that they will be easy to store in a repo and it will be easy to merge them.
```escape

|TestSuiteAbstraction                                               |
|name                                          |execution|iterations|
|tc.Actor.Demo.DemoMobileBrowsingLogin         |         |          |
|tc.Actor.Demo.DemoMobileBrowsingGetImage      |         |          |
|tc.Actor.Demo.DemoMobileBrowsingRightsGetImage|         |          |
|tc.Actor.Demo.DemoMobileBrowsingImageBmp      |         |          |
|tc.Actor.Demo.DemoMobileBrowsingImageGif      |         |          |
|tc.Actor.Demo.DemoMobileBrowsingImageJpg      |         |          |
```
