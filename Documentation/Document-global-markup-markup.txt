# **Introduction - Markup**
## **Description**
The default markup is parsed by the third party [Remarkable](https://www.npmjs.com/package/remarkable), see the [demo](http://jonschlinkert.github.io/remarkable/demo/).

## **Extensions**
Abstractor have done a lot of extensions. There are two different kind of extensions.
1) The Simple Extensions
2) The Complex Extensions

### **The Simple Extensions**
The simple extensions are easy to use. They are built up by a double bracket and *optional* parameters inside parameters. The *optional* parameters are often generated by the extension when it is saved for the first time.
#### **Examples**
1) [Anchor](markup-anchor)
2) [Ie](markup-ie)

### **The Complex Extensions**
The complex extensions needs a little more effort to understand. But some rows of text can generate very complex images. 

#### **Examples**
1) [Sequence Diagram](markup-sequence-diagram)
2) [State Machine](markup-state-machine)
