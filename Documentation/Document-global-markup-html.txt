# **Markup - HTML**
[[DOC-STATUS=PARTLY, ]]

## **Description**
The HTML Markup provides the documentation with some predefined HTML images. 

### **Example Image**

```object

[button](glyphicon-save-file, Save file)
[br]
[button](glyphicon-edit, Edit file)
[br]
[button](glyphicon-arrow-up)
[br]
[log](Success, Mon. 03 Oct 2016 22:52:05 GMT : 901.764, DemoLogLocal[1], Demo success log., Actors-global.demo.log.DemoLogLocal.js)
[br]
[log_start](Mon. 03 Oct 2016 22:52:05 GMT : 901.764)
[br]
[log_end](Success, 0.764ms)
```

### **Example Markup**


## **Definitions**
### **Anchor**
**format:** \'[anchor](`link`)\'
**description:** Bootstrap buttons using [Glyphicons](http://getbootstrap.com/components/#glyphicons).
* `glyphicon name`: the name of the glyphicon to be viewed on the button and classes to the \<span\>.
* `text`: the optional text to be shown after the button. The text will be parsed through [Markup](markup-markup).

**example:** An anchor link
```object

[anchor](aaaaa)
```
### **Button**
**format:** \'[button](`glyphicon name`, [`text`])\'
**description:** Bootstrap buttons using [Glyphicons](http://getbootstrap.com/components/#glyphicons).
* `glyphicon name`: the name of the glyphicon to be viewed on the button and classes to the \<span\>.
* `text`: the optional text to be shown after the button. The text will be parsed through [Markup](markup-markup).

**example:** A save file button.
```object

[button](glyphicon-save-file, Save file)
```

### **Toolbar**
**format:** \'[toolbar](`glyphicon name`, [`text`])\'
```object

[toolbar](actor-editor-project-save,new-group,actor-editor-folder-new))
```

### **Line break**
**format:** \'[br]\'
**description:** Adding a line break to the document inside the HTML-Markup. The html element \<br /\> will be added.
**example:*** A line break
Hello here comes the line break.
```object

[br]
```
Now we are after the line break.



### **Log**
**format:** \'[log](`log type`, `date`, `actor`, `log`, `file name`)\'
**description:** Adding a log row image to the document.
* `log type`: The name of the log type.
  * **Engine:** Engine log.
  * **Debug:** Debug log.
  * **Error:** Error log.
  * **Warning:** Warning log.
  * **IP:** IP log.
  * **Success:** Verification Success log.
  * **Failure:** Verification Failure log.
* `date`: A string. A comma sign must be escaped. '\\,'
* `actor`: A string. A comma sign must be escaped. '\\,'
* `log`: A string. A comma sign must be escaped. '\\,'
* `file name`: A string. A comma sign must be escaped. '\\,'

**example:** A warning log.

```object

[log](Warning, Mon. 03 Oct 2016 22:52:05 GMT : 901.764, DemoLogLocal[1], Demo warninglog., Actors-global.demo.log.DemoLogLocal.js)
```



### **Log Start**
**format:** \'\[log_start\](`date`)\'
**description:** Adding a log start row image to the document.
* `date`: A string. A comma sign must be escaped. '\\,'

**example:** A [Test Case](test-cases) start log.
```object

[log_start](Mon. 03 Oct 2016 22:52:05 GMT : 901.764)
```



### **Log End**
**format:** \'[log_end](`result`, `date`)\'
**description:** Adding a log end row image to the document.
* `result`: A string
  * **None:** 
  * **n/a:** 
  * **Success:** 
  * **n_exec:** 
  * **Interrupt:** 
  * **Error:** 
  * **Failure:** 
  * **Timeout:** 
* `duration`: A string. A comma sign must be escaped. '\\,'

**example:** A [Test Case](test-cases) end log.
```object

[log_end](Error, 0.321ms)
```


